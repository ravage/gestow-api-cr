require "router"
require "yaml"
require "logger"
require "pg"
require "./gestow/repo"
require "./gestow/*"

module Gestow
  class App < ApplicationBase
    def draw
      controller = GPSController.new
      post "/gps" { |context, params| controller.create(context, params) }
    end
  end
end

Gestow::App.run

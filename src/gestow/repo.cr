module Gestow
  PG = DB.open Gestow.config.database.uri
  at_exit { PG.close }

  module Repo
    def insert(values)
      PG.exec(Gestow.config.queries.fetch("insert"),
        values.fetch("towregistry", ""),
        values.fetch("cmd", ""),
        values.fetch("serialnumber", ""),
        values.fetch("datetimeint", 0),
        values.fetch("validity", 0),
        values.fetch("latitude", 0),
        values.fetch("longitude", 0),
        values.fetch("hemispherelat", ""),
        values.fetch("hemispherelong", ""),
        values.fetch("satnumber", 0),
        values.fetch("speed", 0),
        values.fetch("direction", 0),
        values.fetch("baterie", 0),
        values.fetch("gsm", 0),
        values.fetch("roaming", 0),
        values.fetch("acc_ignition", 0),
        values.fetch("block", 0),
        values.fetch("input_1", 0),
        values.fetch("input_2", 0),
        values.fetch("input_3", 0),
        values.fetch("input_4", 0),
        values.fetch("terminal", ""),
      )
    end
  end
end

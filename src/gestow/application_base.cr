module Gestow
  abstract class ApplicationBase
    include Router

    @logger = Logger.new(STDOUT)

    def initialize
      @logger ||= Logger.new(STDOUT)
    end

    def run
      @logger.info("Serving @ #{Gestow.config.server.address}:#{Gestow.config.server.port}")

      server = HTTP::Server.new([
        # HTTP::LogHandler.new(STDOUT),
        HTTP::ErrorHandler.new,
        route_handler,
      ])

      server.bind_tcp(Gestow.config.server.address, Gestow.config.server.port)
      server.listen
    end

    def self.run
      app = App.new
      app.draw
      app.run
    end
  end
end

module Gestow
  @@config : Config?

  def self.config
    @@config = File.open("config.yml") { |file| Config.from_yaml(file) }
  end

  class DatabaseConfig
    YAML.mapping(
      uri: String
    )
  end

  class ServerConfig
    YAML.mapping(
      address: String,
      port: Int32
    )
  end

  class Config
    YAML.mapping(
      database: DatabaseConfig,
      server: ServerConfig,
      queries: Hash(String, String)
    )
  end
end
module Gestow
  struct GPSController
    include Gestow::Repo

    def create(context, params)
      payload = HTTP::Params.parse(context.request.body.not_nil!.gets_to_end)
      insert(payload.to_h)
      context
    end
  end
end
